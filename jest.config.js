module.exports = {
    preset: 'ts-jest',
    testMatch: ["**/__tests__/**/*.tests.[jt]s?(x)"]
};
