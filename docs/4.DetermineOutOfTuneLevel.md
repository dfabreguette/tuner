# Determining the Out-of-Tune Level

At this stage *fundamental* and *harmonic* frequencies of a signal are already found. It is used to find an *summed average distance* to nearest in-tune note frequency. It goes as follows:

1. Calculate nearest in-tune note frequency;
2. Find a difference of of played note and nearest in-tune note frequencies.
3. Average out all the differences.

The result is an accurate measurement of out-of-tune level, because averaging cancels out sampling resolution errors of each harmonic frequency.

To finalize, the result, which is expressed in **ΔHz** units, is converted to [**cents**](https://en.wikipedia.org/wiki/Cent_(music)) - a logarithmic unit of measure, which is much more convenient for tuning, because it gives equal gradation for every note.
