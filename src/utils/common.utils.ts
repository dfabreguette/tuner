export const avg = (arr: Array<number> | Uint8Array) => 
    (arr as Array<number>).reduce((a,b) => a + b, 0) / arr.length;

export const isFirstOccurrence = (el: any, i: number, arr: Array<any>) => 
    i === arr.indexOf(el);

/**
 * Assumes input to be a continuous stream.
 * Returns true once when a continuous truthy stream 
 * is passed to a fn for a `targetDuration`.
 * Passing false at any time resets the timer.
 */
export const initContinuouslyTrueChecker = (targetDuration = 1000) => {
    let startTime = null;
    let isContinuouslyTrue = false;
    
    return (isTrue: boolean) => {
        const currentTime = new Date().getTime();

        if (!startTime && isTrue) {
            startTime = currentTime;
        }

        if (startTime && !isTrue) {
            startTime = null;
            isContinuouslyTrue = false;
        }

        isContinuouslyTrue = !isContinuouslyTrue && 
            startTime && 
            currentTime - startTime >= targetDuration

        return isContinuouslyTrue;
    }
};
