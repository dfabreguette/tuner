import { TGraphData } from "../../components/Tuner/initGraphView";
import { IHarmonic } from "../freq.utils";

export const harmonicsToGraphData = (
    harmonics: Array<IHarmonic>,
    data: Uint8Array,
    width: number,
    height: number
): TGraphData => harmonics.map(h => ({
    x: width * h.index / data.length,
    y: height * h.value / (data.BYTES_PER_ELEMENT * 256)
}));

export const tresholdsToGraphData = (
    harmonics: Array<IHarmonic>,
    data: Uint8Array,
    width: number,
    height: number
): TGraphData => {
    const maxV = data.BYTES_PER_ELEMENT * 256;
    return harmonics.map(h => ({
        x: width * h.index / data.length,
        y: height * Math.min(h.treshold, maxV) / (maxV)
    }));
};

export const int8ArrayToGraphData = (
    data: Uint8Array,
    width: number,
    height: number
): TGraphData => {
    return Array.from(data).map((v, i) => ({
        x: width * i / data.length,
        y: height * v / (data.BYTES_PER_ELEMENT * 256)
    }));
};
