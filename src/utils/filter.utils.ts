export const lowpass = (data: Uint8Array, cutoffI: number) => 
    data.slice(0, cutoffI);

export const bandpass = (data: Uint8Array, fromI: number, toI: number) => 
    data.slice(fromI, toI);

export const downsample = (data: Uint8Array, rate: number) => {
    const length = Math.round(data.length / rate);
    const res = new Uint8Array(length);

    for (let i = 0; i <= length; i++) {
        res[i] = data[i * rate];
    }

    return res;
};
