import { getHarmonics, getNoteIndexRange } from "../freq.utils";
import { mock } from "./mocks/data.mock";


describe('getNoteIndexRange', () => {
    it('returns G# and A# freq indexes for A', () => {
        const solSharpFreq = 415;
        const laFreq = 440;
        const laSharpFreq = 466;
        const [fromI, toI] = getNoteIndexRange(laFreq, 1);
        expect(fromI).toBe(solSharpFreq);
        expect(toI).toBe(laSharpFreq);
    });
})

describe('getHarmonics', () => {
    it('returns second harmonic of small array', () => {
        const h = getHarmonics(new Uint8Array([0, 0, 255, 0, 255]), 110);
        expect(h).toHaveLength(2);
        expect(h[0]).toEqual({index: 2, freq: 220, order: 1, value: 255});
        expect(h[1]).toEqual({index: 4, freq: 440, order: 2, value: 255});
    });

    it('returns second harmonic of big array', () => {
        const h = getHarmonics(mock.doubleSineData, mock.deltaFreq);
        expect(h).toHaveLength(3);
        expect(h[0].index).toBe(150);
        expect(h[1].index).toBe(300);
        expect(h[2].index).toBe(451);
    });

    it ('returns 9 harmonics of a string A signal within error range', () => {
        const errRate = mock.deltaFreq * 4;
        const laIndex = Math.round(110 / mock.deltaFreq);
        const hs = getHarmonics(mock.aStringData, mock.deltaFreq);
        expect(hs).toHaveLength(9);
        hs.forEach((h) => {
            expect(h.index).toBeLessThanOrEqual(laIndex * h.order + errRate);
            expect(h.index).toBeGreaterThanOrEqual(laIndex * h.order - errRate);
        });
    });

    it ('returns 9 harmonics of a string B signal within error range', () => {
        const errRate = mock.deltaFreq * 4;
        const siIndex = Math.round(247 / mock.deltaFreq);
        const hs = getHarmonics(mock.bStringData, mock.deltaFreq);
        expect(hs).toHaveLength(6);
        hs.forEach((h) => {
            expect(h.index).toBeLessThanOrEqual(siIndex * h.order + errRate);
            expect(h.index).toBeGreaterThanOrEqual(siIndex * h.order - errRate);
        });
    });

    it ('returns 0 harmonics of a loud white noise', () => {
        const hs = getHarmonics(mock.loudNoiseData, mock.deltaFreq);
        expect(hs).toHaveLength(0);
    });
});
