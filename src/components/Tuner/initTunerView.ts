import { initAnalogGaugeView } from "./initAnalogGaugeView";
import { initDigitalGaugeView } from "./initDigitalGaugeView";
import css from './styles/tuner.scss';

export const initTunerView = (parent: Element, isAnalogView = false) => {
    const container = document.createElement('div');
    container.classList.add(css.container);
    parent.appendChild(container);

    const turnOnBtn = document.createElement('button');
    turnOnBtn.classList.add(css.turnOnBtn);
    turnOnBtn.innerHTML = 'Turn on';
    container.appendChild(turnOnBtn);

    let gaugeView;
    const setView = (isAnalogView: boolean) => {
        if (gaugeView) {
            container.removeChild(gaugeView.el);
        }

        gaugeView = isAnalogView ? 
            initAnalogGaugeView(container) : 
            initDigitalGaugeView(container);
    };

    setView(isAnalogView);

    const toggleView = () => {
        isAnalogView = !isAnalogView;
        setView(isAnalogView);
    };

    return {
        update: (...a) => gaugeView.update(...a),
        highlight: () => gaugeView.highlight(), 
        setView, 
        toggleView,
        onTurnOnClick: cb => turnOnBtn.addEventListener('click', cb),
        hideTurnOnButton: () => container.removeChild(turnOnBtn),
        showLoader: () => {
            turnOnBtn.innerHTML = 'Loading...';
            turnOnBtn.toggleAttribute('disabled', true);
        }
    };
};
