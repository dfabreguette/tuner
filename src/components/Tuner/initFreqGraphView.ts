import { initGraphView } from "./initGraphView"
import { int8ArrayToGraphData, harmonicsToGraphData, tresholdsToGraphData } from "../../utils/misc/int8ArrayToGraphData";
import { IHarmonic } from "../../utils/freq.utils";
import { avg } from "../../utils/common.utils";

export const initFreqGraphView = (parent: Element) => {
    const graph = initGraphView(parent);
    return {
        draw: (
            data: Uint8Array, 
            harmonics: Array<IHarmonic>
        ) => {
            const points = int8ArrayToGraphData(
                data, 
                graph.canvas.width, 
                graph.canvas.height
            );

            const harmonicPoints = harmonicsToGraphData(
                harmonics,
                data,
                graph.canvas.width, 
                graph.canvas.height
            );

            const tresholdPoints = tresholdsToGraphData(
                harmonics,
                data,
                graph.canvas.width, 
                graph.canvas.height
            );

            graph.clear();
            graph.drawPath(points);
            graph.drawPoints(harmonicPoints, 'red');
            graph.drawPoints(tresholdPoints, 'lightgreen', 16, 2);

            const avgV = avg(data) * graph.canvas.height / 256;
            if (avgV > 0) {
                graph.drawPath([
                    {x: 0, y: avgV}, 
                    {x: graph.canvas.width, y: avgV}
                ], 'rgba(200, 200, 200, .5)');   
            }
        },
        canvas: graph.canvas
    }
}
