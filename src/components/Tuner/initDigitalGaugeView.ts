import css from './styles/digitalGauge.scss';
import { ITuningInfo } from "../../getTuningInfo";
import { initFreqGraphView } from './initFreqGraphView';

const getCentStr = (cents: number) => !isNaN(cents) ?
    `${cents >= 0 ? '+' : '-'}${Math.abs(cents)}¢` :
    ''

export const initDigitalGaugeView = (parent: Element) => {
    const container = document.createElement('div');
    parent.appendChild(container);
    
    const dashboardEl = document.createElement('div');
    dashboardEl.classList.add(css.dashboard);
    container.appendChild(dashboardEl);

    const noteEl = document.createElement('div');
    dashboardEl.appendChild(noteEl);
    
    const centsEl = document.createElement('div');
    dashboardEl.appendChild(centsEl);
    
    const harmonicsEl = document.createElement('div');
    harmonicsEl.classList.add(css.harmonics);
    dashboardEl.appendChild(harmonicsEl);

    const gaugeEl = document.createElement('div');
    gaugeEl.classList.add(css.gauge);
    container.appendChild(gaugeEl);

    const handEl = document.createElement('div');
    handEl.classList.add(css.hand);
    gaugeEl.appendChild(handEl);
    
    const setHand = (info: ITuningInfo) => {        
        info.isInTune ? 
            handEl.classList.add(css.inTune) :
            handEl.classList.remove(css.inTune);

        handEl.setAttribute('style', `left: ${50 + info.cents}%;`);
    }

    const freqGraph = initFreqGraphView(container);
    freqGraph.canvas.classList.add(css.graph);

    return {
        update: (info: ITuningInfo, data: Uint8Array) => {
            if (noteEl.innerHTML !== info.noteStr) 
                noteEl.innerHTML = info.noteStr;
                    
            const harmonicsText = `h: ${info.harmonics.length}`;
            if (harmonicsEl.innerHTML !== harmonicsText) 
                harmonicsEl.innerHTML = harmonicsText;

            const centStr = getCentStr(info.cents);        
            if (centsEl.innerHTML !== centStr)
                centsEl.innerHTML = centStr;
            
            setHand(info);
            
            freqGraph.draw(data, info.harmonics);
        },
        highlight: () => {
            handEl.classList.add(css.highlighted);
            setTimeout(() => handEl.classList.remove(css.highlighted), 250);
        },
        el: container
    }
};
